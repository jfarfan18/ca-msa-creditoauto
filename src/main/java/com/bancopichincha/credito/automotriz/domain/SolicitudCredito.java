package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enums.EstadoSolicitudCredito;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SolicitudCredito {

    @Id
    @GeneratedValue
    private Long id;

    private Date fechaElaboracion;

    private Integer mesesPlazo;

    private BigDecimal cuota;

    private BigDecimal entrada;

    @ManyToOne()
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @ManyToOne()
    @JoinColumn(name = "patio_id")
    private Patio patio;

    @ManyToOne()
    @JoinColumn(name = "vehiculo_id")
    private Vehiculo vehiculo;

    @ManyToOne()
    @JoinColumn(name = "ejecutivo_id")
    private Ejecutivo ejecutivo;

    @Enumerated(EnumType.STRING)
    private EstadoSolicitudCredito estadoSolicitud;

    private String observacion;

}
