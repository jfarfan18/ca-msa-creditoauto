package com.bancopichincha.credito.automotriz.domain.enums;

public enum EstadoSolicitudCredito {
    REGISTRADA, DESPACHADA, CANCELADA;
}
