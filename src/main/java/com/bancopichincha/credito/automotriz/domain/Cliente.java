package com.bancopichincha.credito.automotriz.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private boolean sujetoCredito;

    @ManyToOne()
    @JoinColumn(name = "persona_id")
    private Persona persona;

    public Cliente() {
    }

    public Cliente(Long id, boolean sujetoCredito, Persona persona) {
        this.id = id;
        this.sujetoCredito = sujetoCredito;
        this.persona = persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSujetoCredito() {
        return sujetoCredito;
    }

    public void setSujetoCredito(boolean sujetoCredito) {
        this.sujetoCredito = sujetoCredito;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
