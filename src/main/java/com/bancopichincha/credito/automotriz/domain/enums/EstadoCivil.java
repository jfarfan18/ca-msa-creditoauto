package com.bancopichincha.credito.automotriz.domain.enums;

public enum EstadoCivil {
    SOLTERO,CASADO,DIVORCIADO,UNION_LIBRE;
}
