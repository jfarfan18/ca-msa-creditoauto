package com.bancopichincha.credito.automotriz.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Getter
@Setter
public class MarcaDto {

    private Long id;

    @NotEmpty
    private String nombre;


}
