package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.Persona;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
public class EjecutivoDto {

    private Long id;

    private Long idPatio;

    private String nombrePatio;

    private Long idPersona;

    private String identificacion;

    private String nombres;

    private String apellidos;

}
