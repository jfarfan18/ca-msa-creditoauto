package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.exception.RecordInUseException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.repository.EjecutivoRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.service.PatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatioServiceImpl implements PatioService {

    @Autowired
    private PatioRepository patioRepository;

    @Autowired
    private EjecutivoRepository ejecutivoRepository;
    @Autowired
    private SolicitudCreditoRepository solicitudCreditoRepository;

    @Autowired
    private AsignacionClienteRepository asignacionClienteRepository;

    @Override
    public Patio create(Patio patio) {
        return patioRepository.save(patio);
    }

    @Override
    public Patio update(Long id, Patio patio) {
        Optional<Patio> optionalPatio=patioRepository.findById(id);
        if(!optionalPatio.isPresent()){
            throw new RecordNotFoundException(String.format("Patio con identificador %s no existe",id));
        }
        Patio patioDb=optionalPatio.get();
        patioDb.setDireccion(patio.getDireccion());
        patioDb.setNombre(patio.getNombre());
        patioDb.setTelefono(patio.getTelefono());
        patioDb.setNroPuntoVenta(patio.getNroPuntoVenta());
        return patioRepository.save(patioDb);
    }

    @Override
    public void delete(Long id) {
        Optional<Patio> optionalPatio=patioRepository.findById(id);
        if(!optionalPatio.isPresent()){
            throw new RecordNotFoundException(String.format("Patio con identificador %s no existe",id));
        }
        this.validatePatioNotInUse(optionalPatio.get());
        patioRepository.delete(optionalPatio.get());
    }

    @Override
    public Patio findById(Long id) {
        Optional<Patio> optionalPatio = patioRepository.findById(id);
        if(!optionalPatio.isPresent()){
            throw new RecordNotFoundException(String.format("Patio con identificador %s no existe",id));
        }
        return optionalPatio.get();
    }

    @Override
    public List<Patio> findAll() {
        return patioRepository.findAll();
    }

    private void validatePatioNotInUse(Patio patio){
        List<SolicitudCredito> solicitudes = solicitudCreditoRepository.findByPatio(patio);
        List<Ejecutivo> ejecutivos = ejecutivoRepository.findByPatio(patio);
        List<AsignacionCliente> asignacionesCliente = asignacionClienteRepository.findByPatio(patio);
        if((solicitudes!=null && !solicitudes.isEmpty())
        ||(ejecutivos!=null && !ejecutivos.isEmpty()
        ||(asignacionesCliente!=null && !asignacionesCliente.isEmpty()))){
            throw new RecordInUseException(String.format("No es posible eliminar el vehículo con identificador %s ya que tiene información asociada",patio.getId()));
        }
    }
}
