package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;

import java.util.List;

public interface SolicitudCreditoService {

    public SolicitudCredito create(SolicitudCredito solicitudCredito);

    public SolicitudCredito update(Long id, SolicitudCredito solicitudCredito);

    public void delete(Long id);

    public SolicitudCredito findById(Long id);

    public List<SolicitudCredito> findAll();
}
