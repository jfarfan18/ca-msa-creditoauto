package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Cliente;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class AsignacionClienteDto {

    private Long id;

    @NotNull
    private Long idCliente;

    private String identificacionCliente;

    private String nombresCliente;

    private String apellidosCliente;

    @NotNull
    private Long idPatio;

    private String nombrePatio;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date fechaAsignacion;


}
