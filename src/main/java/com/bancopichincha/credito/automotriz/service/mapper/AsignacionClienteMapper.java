package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.service.dto.AsignacionClienteDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AsignacionClienteMapper {

    @Mapping(source = "cliente.id",target = "idCliente")
    @Mapping(source = "cliente.persona.identificacion",target = "identificacionCliente")
    @Mapping(source = "cliente.persona.nombres",target = "nombresCliente")
    @Mapping(source = "cliente.persona.apellidos",target = "apellidosCliente")
    @Mapping(source = "patio.id",target = "idPatio")
    @Mapping(source = "patio.nombre",target = "nombrePatio")
    AsignacionClienteDto asignacionClienteToAsignacionClienteDto(AsignacionCliente asignacionCliente);

    @Mapping(source = "idCliente",target = "cliente.id")
    @Mapping(source = "identificacionCliente",target = "cliente.persona.identificacion")
    @Mapping(source = "nombresCliente",target = "cliente.persona.nombres")
    @Mapping(source = "apellidosCliente",target = "cliente.persona.apellidos")
    @Mapping(source = "idPatio",target = "patio.id")
    @Mapping(source = "nombrePatio",target = "patio.nombre")
    AsignacionCliente asignacionClienteDtoToAsignacionCliente(AsignacionClienteDto asignacionClienteDto);

    List<AsignacionClienteDto> asignacionClienteToAsignacionClienteDtoAllDtos(List<AsignacionCliente> asignacionClientes);

}
