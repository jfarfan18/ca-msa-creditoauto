package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Cliente;
import com.bancopichincha.credito.automotriz.domain.Ejecutivo;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.domain.enums.EstadoSolicitudCredito;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class SolicitudCreditoDto {

    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date fechaElaboracion;

    @NotNull
    private Integer mesesPlazo;

    @NotNull
    @Positive
    private BigDecimal cuota;

    @NotNull
    @PositiveOrZero
    private BigDecimal entrada;

    @NotNull
    private Long idCliente;

    private String identificacionCliente;
    private String nombresCliente;
    private String apellidosCliente;

    @NotNull
    private Long idPatio;

    private String nombrePatio;

    @NotNull
    private Long idVehiculo;

    private String placa;

    @NotNull
    private Long idEjecutivo;

    private String identificacionEjecutivo;
    private String nombresEjecutivo;
    private String apellidosEjecutivo;

    @NotNull
    @Enumerated(EnumType.STRING)
    private EstadoSolicitudCredito estadoSolicitud;

    private String observacion;


}
