package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Persona;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ClienteDto {

    private Long id;

    @NotNull
    private boolean sujetoCredito;

    private PersonaDto persona;
}
