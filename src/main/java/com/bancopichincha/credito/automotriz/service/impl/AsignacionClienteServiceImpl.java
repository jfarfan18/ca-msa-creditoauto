package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.service.AsignacionClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AsignacionClienteServiceImpl implements AsignacionClienteService {

    @Autowired
    AsignacionClienteRepository asignacionClienteRepository;

    @Override
    public AsignacionCliente create(AsignacionCliente asignacionCliente) {
        return asignacionClienteRepository.save(asignacionCliente);
    }

    @Override
    public AsignacionCliente update(Long id, AsignacionCliente asignacionCliente) {
        Optional<AsignacionCliente> asignacionClienteOptional=asignacionClienteRepository.findById(id);
        if(!asignacionClienteOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Asignacion Cliente con identificador %s no existe",id));
        }
        AsignacionCliente asignacionClienteDb=asignacionClienteOptional.get();
        asignacionClienteDb.setCliente(asignacionCliente.getCliente());
        asignacionClienteDb.setPatio(asignacionCliente.getPatio());
        asignacionClienteDb.setFechaAsignacion(asignacionCliente.getFechaAsignacion());
        return asignacionClienteRepository.save(asignacionClienteDb);
    }

    @Override
    public void delete(Long id) {
        Optional<AsignacionCliente> asignacionClienteOptional=asignacionClienteRepository.findById(id);
        if(!asignacionClienteOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Asignacion Cliente con identificador %s no existe",id));
        }
        asignacionClienteRepository.delete(asignacionClienteOptional.get());
    }

    @Override
    public AsignacionCliente findById(Long id) {
        Optional<AsignacionCliente> asignacionClienteOptional = asignacionClienteRepository.findById(id);
        if(!asignacionClienteOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Asignacion Cliente con identificador %s no existe",id));
        }
        return asignacionClienteOptional.get();
    }

    @Override
    public List<AsignacionCliente> findAll() {
        return asignacionClienteRepository.findAll();
    }
}
