package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.service.dto.VehiculoDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface VehiculoMapper {


    @Mapping(source = "marca.id",target = "idMarca")
    @Mapping(source = "marca.nombre",target = "nombreMarca")
    VehiculoDto vehiculoToVehiculoDto(Vehiculo vehiculo);

    @Mapping(source = "idMarca",target = "marca.id")
    @Mapping(source = "nombreMarca",target = "marca.nombre")
    Vehiculo VehiculoDtoToVehiculo(VehiculoDto vehiculoDto);

    List<VehiculoDto> vehiculoToVehiculoDtoAllDtos(List<Vehiculo> vehiculos);

}
