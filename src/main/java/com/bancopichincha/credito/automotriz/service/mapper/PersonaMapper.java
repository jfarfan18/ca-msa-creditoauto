package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Persona;
import com.bancopichincha.credito.automotriz.service.dto.PersonaDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonaMapper {
    PersonaDto personaToPersonaDto(Persona persona);

    Persona PersonaDtoToPersona(PersonaDto personaDto);

    List<PersonaDto> personaToPersonaDtoAllDtos(List<Persona> personas);

}
