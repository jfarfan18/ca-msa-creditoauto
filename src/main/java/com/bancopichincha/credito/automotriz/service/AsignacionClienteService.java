package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;

import java.util.List;
import java.util.Optional;

public interface AsignacionClienteService {

    public AsignacionCliente create(AsignacionCliente asignacionCliente);

    public AsignacionCliente update(Long id, AsignacionCliente asignacionCliente);

    public void delete(Long id);

    public AsignacionCliente findById(Long id);

    public List<AsignacionCliente> findAll();
}
