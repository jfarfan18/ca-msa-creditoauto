package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.domain.Ejecutivo;
import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.domain.enums.EstadoSolicitudCredito;
import com.bancopichincha.credito.automotriz.exception.SolicitudRegistrationFailException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.repository.EjecutivoRepository;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.service.SolicitudCreditoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SolicitudCreditoServiceImpl implements SolicitudCreditoService {

    @Autowired
    private SolicitudCreditoRepository solicitudCreditoRepository;

    @Autowired
    private EjecutivoRepository ejecutivoRepository;

    @Autowired
    private AsignacionClienteRepository asignacionClienteRepository;

    @Override
    @Transactional
    public SolicitudCredito create(SolicitudCredito solicitudCredito) {
        this.validateOnlyOneSolicitudPerDay(solicitudCredito);
        this.validateEjecutivoSamePatioSolicitud(solicitudCredito);
        this.validateAvailableVehiculo(solicitudCredito);
        AsignacionCliente asignacionCliente=getAsignacionCliente(solicitudCredito);
        asignacionClienteRepository.save(asignacionCliente);
        return solicitudCreditoRepository.save(solicitudCredito);
    }

    @Override
    public SolicitudCredito update(Long id, SolicitudCredito solicitudCredito) {
        this.validateOnlyOneSolicitudPerDay(solicitudCredito);
        this.validateEjecutivoSamePatioSolicitud(solicitudCredito);
        this.validateAvailableVehiculo(solicitudCredito);
        Optional<SolicitudCredito> solicitudCreditoOptional=solicitudCreditoRepository.findById(id);
        if(!solicitudCreditoOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Solicitd de crédito con identificador %s no existe",id));
        }
        SolicitudCredito solicitudCreditoDb=solicitudCreditoOptional.get();
        solicitudCreditoDb.setEstadoSolicitud(solicitudCredito.getEstadoSolicitud());
        solicitudCreditoDb.setCliente(solicitudCredito.getCliente());
        solicitudCreditoDb.setCuota(solicitudCredito.getCuota());
        solicitudCreditoDb.setEjecutivo(solicitudCredito.getEjecutivo());
        solicitudCreditoDb.setEntrada(solicitudCredito.getEntrada());
        solicitudCreditoDb.setFechaElaboracion(solicitudCredito.getFechaElaboracion());
        solicitudCreditoDb.setMesesPlazo(solicitudCredito.getMesesPlazo());
        solicitudCreditoDb.setPatio(solicitudCredito.getPatio());
        solicitudCreditoDb.setVehiculo(solicitudCredito.getVehiculo());
        solicitudCreditoDb.setObservacion(solicitudCredito.getObservacion());
        return solicitudCreditoRepository.save(solicitudCreditoDb);
    }

    @Override
    public void delete(Long id) {
        Optional<SolicitudCredito> solicitudCreditoOptional=solicitudCreditoRepository.findById(id);
        if(!solicitudCreditoOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Solicitud de crédito con identificador %s no existe",id));
        }
        solicitudCreditoRepository.delete(solicitudCreditoOptional.get());
    }

    @Override
    public SolicitudCredito findById(Long id) {
        Optional<SolicitudCredito> optionalSolicitudCredito = solicitudCreditoRepository.findById(id);
        if(!optionalSolicitudCredito.isPresent()){
            throw new RecordNotFoundException(String.format("Solicitud de Crédito con identificador %s no existe",id));
        }
        return optionalSolicitudCredito.get();
    }

    @Override
    public List<SolicitudCredito> findAll() {
        return solicitudCreditoRepository.findAll();
    }

    private void validateOnlyOneSolicitudPerDay(SolicitudCredito solicitudCredito){
        List<SolicitudCredito> solicitudes = solicitudCreditoRepository.findByFechaElaboracionAndCliente(solicitudCredito.getFechaElaboracion(),
                solicitudCredito.getCliente());
        if(!solicitudes.isEmpty()){
            throw new SolicitudRegistrationFailException("Ya existe una solicitud creada para el cliente en la fecha ingresada");
        }
    }

    private void validateEjecutivoSamePatioSolicitud(SolicitudCredito solicitudCredito){
        Optional<Ejecutivo> ejecutivo = ejecutivoRepository.findById(solicitudCredito.getEjecutivo().getId());
        if(!ejecutivo.isPresent()){
            throw new RecordNotFoundException(String.format("No existe el ejecutivo con identificador %s",solicitudCredito.getEjecutivo().getId()));
        }
        if(!solicitudCredito.getPatio().getId().equals(ejecutivo.get().getPatio().getId())){
            throw new SolicitudRegistrationFailException("El ejecutivo no pertenece al patio asignado a la solicitud");
        }
    }

    private void validateAvailableVehiculo(SolicitudCredito solicitudCredito){
        List<SolicitudCredito> solicitudes = solicitudCreditoRepository.findByVehiculoAndEstadoSolicitud(solicitudCredito.getVehiculo(), EstadoSolicitudCredito.REGISTRADA);
        if(!solicitudes.isEmpty()){
            throw new SolicitudRegistrationFailException("El vehículo se encuentra reservado");
        }
    }

    private AsignacionCliente getAsignacionCliente(SolicitudCredito solicitudCredito){
        Optional<AsignacionCliente> asignacionDb = asignacionClienteRepository.findByClienteAndPatio(solicitudCredito.getCliente(), solicitudCredito.getPatio());
        if(asignacionDb.isPresent()){
            return asignacionDb.get();
        }
        return new AsignacionCliente(null,solicitudCredito.getCliente(),solicitudCredito.getPatio(),new Date());
    }
}
