package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.Vehiculo;

import java.util.List;
import java.util.Optional;

public interface VehiculoService {

    public Vehiculo create(Vehiculo vehiculo);

    public Vehiculo update(Long id, Vehiculo vehiculo);

    public List<Vehiculo> findByMarcaAndModeloAndAnio(Long idMarca, String modelo, Integer anio);

    public void delete(Long id);

    public Vehiculo findById(Long id);

    public List<Vehiculo> findAll();
}
