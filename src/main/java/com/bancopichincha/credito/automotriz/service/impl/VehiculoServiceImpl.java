package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.exception.RecordInUseException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.exception.UniqueViolationException;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.repository.VehiculoRepository;
import com.bancopichincha.credito.automotriz.service.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehiculoServiceImpl implements VehiculoService {

    @Autowired
    VehiculoRepository vehiculoRepository;

    @Autowired
    SolicitudCreditoRepository solicitudCreditoRepository;

    @Override
    public Vehiculo create(Vehiculo vehiculo) {
        this.validateUniquePlaca(vehiculo);
        return vehiculoRepository.save(vehiculo);
    }

    @Override
    public Vehiculo update(Long id, Vehiculo vehiculo) {
        Optional<Vehiculo> vehiculoOptional=vehiculoRepository.findById(id);
        if(!vehiculoOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Vehículo con identificador %s no existe",id));
        }
        Vehiculo vehiculoDb=vehiculoOptional.get();
        vehiculoDb.setAnio(vehiculo.getAnio());
        vehiculoDb.setAvaluo(vehiculo.getAvaluo());
        vehiculoDb.setCilindraje(vehiculo.getCilindraje());
        vehiculoDb.setMarca(vehiculo.getMarca());
        vehiculoDb.setModelo(vehiculo.getModelo());
        vehiculoDb.setNroChasis(vehiculo.getNroChasis());
        vehiculoDb.setPlaca(vehiculo.getPlaca());
        vehiculoDb.setTipo(vehiculo.getTipo());
        return vehiculoRepository.save(vehiculoDb);
    }

    @Override
    public List<Vehiculo> findByMarcaAndModeloAndAnio(Long idMarca, String modelo, Integer anio) {
        return vehiculoRepository.consultaMarcaModeloAnio(idMarca,modelo,anio);
    }

    @Override
    public void delete(Long id) {
        Optional<Vehiculo> vehiculoOptional=vehiculoRepository.findById(id);
        if(!vehiculoOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Vehículo con identificador %s no existe",id));
        }
        this.validateVehiculoNotInUse(vehiculoOptional.get());
        vehiculoRepository.delete(vehiculoOptional.get());
    }

    @Override
    public Vehiculo findById(Long id) {
        Optional<Vehiculo> vehiculoOptional=vehiculoRepository.findById(id);
        if(!vehiculoOptional.isPresent()){
            throw new RecordNotFoundException(String.format("Vehículo con identificador %s no existe",id));
        }
        return vehiculoOptional.get();
    }

    @Override
    public List<Vehiculo> findAll() {
        return vehiculoRepository.findAll();
    }

    private void validateUniquePlaca(Vehiculo vehiculo){
        Optional<Vehiculo> vehiculoOptional=vehiculoRepository.findByPlaca(vehiculo.getPlaca());
        if(vehiculoOptional.isPresent() && !vehiculo.equals(vehiculoOptional.get())){
            throw new UniqueViolationException(String.format("Ya existe un vehículo con placa %s",vehiculo.getPlaca()));
        }
    }

    private void validateVehiculoNotInUse(Vehiculo vehiculo){
        List<SolicitudCredito> solicitudes = solicitudCreditoRepository.findByVehiculo(vehiculo);
        if(solicitudes!=null && !solicitudes.isEmpty()){
            throw new RecordInUseException(String.format("No es posible eliminar el vehículo con identificador %s ya que tiene informaci{on asociada",vehiculo.getId()));
        }
    }
}
