package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.EstadoCivil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
public class PersonaDto {

    private Long id;

    @NotEmpty
    private String identificacion;

    @NotEmpty
    private String nombres;

    private String apellidos;

    private String telefonoConvencional;

    private String telefonoCelular;

    private String direccion;

    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechaNacimiento;

    @Enumerated(EnumType.STRING)
    private EstadoCivil estadoCivil;

    @ManyToOne()
    @JoinColumn(name = "persona_id")
    private PersonaDto conyuge;

}
