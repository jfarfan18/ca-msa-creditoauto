package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.service.dto.SolicitudCreditoDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SolicitudCreditoMapper {
    @Mapping(source = "cliente.id",target = "idCliente")
    @Mapping(source = "cliente.persona.identificacion",target = "identificacionCliente")
    @Mapping(source = "cliente.persona.nombres",target = "nombresCliente")
    @Mapping(source = "cliente.persona.apellidos",target = "apellidosCliente")
    @Mapping(source = "patio.id",target = "idPatio")
    @Mapping(source = "patio.nombre",target = "nombrePatio")
    @Mapping(source = "vehiculo.id",target = "idVehiculo")
    @Mapping(source = "vehiculo.placa",target = "placa")
    @Mapping(source = "ejecutivo.id",target = "idEjecutivo")
    @Mapping(source = "ejecutivo.persona.identificacion",target = "identificacionEjecutivo")
    @Mapping(source = "ejecutivo.persona.nombres",target = "nombresEjecutivo")
    @Mapping(source = "ejecutivo.persona.apellidos",target = "apellidosEjecutivo")
    SolicitudCreditoDto solicitudCreditoToSolicitudCreditoDto(SolicitudCredito solicitudCredito);

    @Mapping(source = "idCliente",target = "cliente.id")
    @Mapping(source = "identificacionCliente",target = "cliente.persona.identificacion")
    @Mapping(source = "nombresCliente",target = "cliente.persona.nombres")
    @Mapping(source = "apellidosCliente",target = "cliente.persona.apellidos")
    @Mapping(source = "idPatio",target = "patio.id")
    @Mapping(source = "nombrePatio",target = "patio.nombre")
    @Mapping(source = "idVehiculo",target = "vehiculo.id")
    @Mapping(source = "placa",target = "vehiculo.placa")
    @Mapping(source = "idEjecutivo",target = "ejecutivo.id")
    @Mapping(source = "identificacionEjecutivo",target = "ejecutivo.persona.identificacion")
    @Mapping(source = "nombresEjecutivo",target = "ejecutivo.persona.nombres")
    @Mapping(source = "apellidosEjecutivo",target = "ejecutivo.persona.apellidos")
    SolicitudCredito SolicitudCreditoDtoToSolicitudCredito(SolicitudCreditoDto solicitudCreditoDto);

    List<SolicitudCreditoDto> solicitudCreditoToSolicitudCreditoDtoAllDtos(List<SolicitudCredito> solicitudCreditos);
    
}
