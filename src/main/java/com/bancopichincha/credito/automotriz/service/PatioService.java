package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.Patio;

import java.util.List;

public interface PatioService {

    public Patio create(Patio patio);

    public Patio update(Long id, Patio patio);

    public void delete(Long id);

    public Patio findById(Long id);

    public List<Patio> findAll();
}
