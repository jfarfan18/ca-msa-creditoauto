package com.bancopichincha.credito.automotriz.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;


@Getter
@Setter
public class PatioDto {

    private Long id;

    @NotEmpty
    private String nombre;

    @NotEmpty
    private String direccion;

    @NotEmpty
    private String telefono;

    @NotEmpty
    private String nroPuntoVenta;

}
