package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.Marca;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
public class VehiculoDto {

    private Long id;

    @NotEmpty
    private String placa;

    @NotEmpty
    private String modelo;

    @NotEmpty
    private String nroChasis;

    @NotNull
    @Positive
    private Integer anio;

    private String tipo;

    @NotNull
    private Integer cilindraje;

    @NotNull
    @Positive
    private BigDecimal avaluo;

    @NotNull
    private Long idMarca;

    private String nombreMarca;


}
