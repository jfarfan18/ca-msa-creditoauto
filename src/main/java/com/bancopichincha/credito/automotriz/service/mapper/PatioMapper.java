package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatioMapper {
    PatioDto patioToPatioDto(Patio patio);

    Patio PatioDtoToPatio(PatioDto patioDto);

    List<PatioDto> patioToPatioDtoAllDtos(List<Patio> patios);

}
