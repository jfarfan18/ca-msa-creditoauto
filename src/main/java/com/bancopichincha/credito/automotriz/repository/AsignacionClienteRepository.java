package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.domain.Cliente;
import com.bancopichincha.credito.automotriz.domain.Patio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AsignacionClienteRepository extends JpaRepository<AsignacionCliente,Long> {

    public List<AsignacionCliente> findByPatio(Patio patio);

    Optional<AsignacionCliente> findByClienteAndPatio(Cliente cliente, Patio patio);

}
