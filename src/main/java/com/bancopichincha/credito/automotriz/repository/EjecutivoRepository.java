package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Ejecutivo;
import com.bancopichincha.credito.automotriz.domain.Patio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface EjecutivoRepository extends JpaRepository<Ejecutivo, Long> {

    public List<Ejecutivo> findByPatio(Patio patio);
}
