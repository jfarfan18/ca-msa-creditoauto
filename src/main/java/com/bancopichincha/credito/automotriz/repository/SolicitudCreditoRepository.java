package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Cliente;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.domain.enums.EstadoSolicitudCredito;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SolicitudCreditoRepository extends JpaRepository<SolicitudCredito,Long> {

    public List<SolicitudCredito> findByVehiculo(Vehiculo vehiculo);

    public List<SolicitudCredito> findByPatio(Patio patio);

    public List<SolicitudCredito> findByFechaElaboracionAndCliente(Date fechaElaboracion, Cliente cliente);

    public List<SolicitudCredito> findByVehiculoAndEstadoSolicitud(Vehiculo vehiculo, EstadoSolicitudCredito estadoSolicitud);

}
