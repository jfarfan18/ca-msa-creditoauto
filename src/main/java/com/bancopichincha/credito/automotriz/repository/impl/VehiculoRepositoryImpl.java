package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.repository.VehiculoRepositoryCustom;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehiculoRepositoryImpl implements VehiculoRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Vehiculo> consultaMarcaModeloAnio(Long idMarca, String modelo, Integer anio) {
        CriteriaBuilder builder =entityManager.getCriteriaBuilder();
        CriteriaQuery<Vehiculo> criteria = builder.createQuery(Vehiculo.class);
        Root<Vehiculo> root = criteria.from(Vehiculo.class);
        Join<Object, Object> marca = root.join("marca", JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();
        if(idMarca!=null){
            predicates.add(builder.and(builder.equal(marca.get("id"),idMarca)));
        }
        if(modelo!=null && !modelo.isEmpty()){
            predicates.add(builder.and(builder.equal(root.get("modelo"),modelo)));
        }
        if(anio!=null){
            predicates.add(builder.and(builder.equal(root.get("anio"),anio)));
        }

        criteria.select(root).where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
        TypedQuery<Vehiculo> query = entityManager.createQuery(criteria);
        return query.getResultList();
    }

}
