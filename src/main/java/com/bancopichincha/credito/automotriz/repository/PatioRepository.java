package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Patio;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PatioRepository extends JpaRepository<Patio, Long> {

}
