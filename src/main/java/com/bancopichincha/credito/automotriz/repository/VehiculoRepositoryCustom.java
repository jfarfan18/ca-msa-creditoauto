package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehiculo;

import java.util.List;

public interface VehiculoRepositoryCustom {

    List<Vehiculo> consultaMarcaModeloAnio (Long idMarca, String modelo, Integer anio);
}
