package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.repository.*;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Profile("!test")
public class CommandLineTaskExecutor implements CommandLineRunner {

    @Autowired
    private MarcaRepository marcaRepository;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private EjecutivoRepository ejecutivoRepository;

    @Autowired
    private PatioRepository patioRepository;

    @Autowired
    private AsignacionClienteRepository asignacionClienteRepository;

    @Autowired
    private VehiculoRepository vehiculoRepository;

    @Autowired
    private SolicitudCreditoRepository solicitudCreditoRepository;

    @Override
    public void run(String... args) throws Exception {
        cleanDb();
        readMarcasCsv();
        readClientesCsv();
        readEjecutivosCsv();
    }

    private void cleanDb(){
        solicitudCreditoRepository.deleteAll();;
        vehiculoRepository.deleteAll();
        asignacionClienteRepository.deleteAll();
        ejecutivoRepository.deleteAll();
        clienteRepository.deleteAll();
        marcaRepository.deleteAll();
        patioRepository.deleteAll();
        personaRepository.deleteAll();
    }

    private List<Marca> readMarcasCsv() throws IOException {
        CSVReader reader=new CSVReaderBuilder(new FileReader(new File("").getAbsolutePath()+"/src/main/resources/marcas.csv")).
                withSkipLines(1). // Skiping firstline as it is header
                        build();
        List<Marca> marcaList=reader.readAll().stream().map(data-> {
            Marca marca= new Marca();
            marca.setNombre(data[0]);
            return marca;
        }).distinct().collect(Collectors.toList());
        return marcaRepository.saveAll(marcaList);
    }

    private List<Cliente> readClientesCsv() throws IOException {
        CSVReader reader=new CSVReaderBuilder(new FileReader(new File("").getAbsolutePath()+"/src/main/resources/clientes.csv")).
                withSkipLines(1). // Skiping firstline as it is header
                        build();
        List<Cliente> clienteList=reader.readAll().stream().map(data-> {
            Cliente cliente=new Cliente();
            cliente.setPersona(new Persona());
            cliente.getPersona().setIdentificacion(data[0]);
            cliente.getPersona().setNombres(data[1]);
            try {
                cliente.getPersona().setFechaNacimiento(new SimpleDateFormat("MM/dd/yyyy").parse(data[3]));
            } catch (ParseException e) {
            }
            cliente.getPersona().setApellidos(data[4]);
            cliente.getPersona().setDireccion(data[5]);
            cliente.getPersona().setTelefonoConvencional(data[6]);
            if(data[7]!=null && !data[7].isEmpty()){
                cliente.getPersona().setConyuge(new Persona());
                cliente.getPersona().getConyuge().setIdentificacion(data[7]);
                cliente.getPersona().getConyuge().setNombres(data[8]);
            }
            cliente.setSujetoCredito(Boolean.parseBoolean(data[9]));
            return cliente;
        }).distinct().collect(Collectors.toList());
        List<Persona> personaList=clienteList.stream().map(c->c.getPersona()).collect(Collectors.toList());
        personaList.addAll(personaList.stream().filter(p->p.getConyuge()!=null).collect(Collectors.toList()));
        personaRepository.saveAll(personaList.stream().distinct().collect(Collectors.toList()));
        return clienteRepository.saveAll(clienteList);
    }

    private List<Ejecutivo> readEjecutivosCsv() throws IOException {
        CSVReader reader=new CSVReaderBuilder(new FileReader(new File("").getAbsolutePath()+"/src/main/resources/ejecutivos.csv")).
                withSkipLines(1). // Skiping firstline as it is header
                        build();
        List<Ejecutivo> ejecutivoList=reader.readAll().stream().map(data-> {
            Ejecutivo ejecutivo=new Ejecutivo();
            ejecutivo.setPersona(new Persona());
            ejecutivo.getPersona().setIdentificacion(data[0]);
            ejecutivo.getPersona().setNombres(data[1]);
            ejecutivo.getPersona().setApellidos(data[2]);
            ejecutivo.getPersona().setDireccion(data[3]);
            ejecutivo.getPersona().setTelefonoConvencional(data[4]);
            ejecutivo.getPersona().setTelefonoCelular(data[5]);
            ejecutivo.setPatio(new Patio(null,data[6],"","",""));
            return ejecutivo;
        }).distinct().collect(Collectors.toList());
        List<Persona> personaList=ejecutivoList.stream().map(c->c.getPersona()).collect(Collectors.toList());
        personaRepository.saveAll(personaList.stream().distinct().collect(Collectors.toList()));
        List<Patio> patioList=ejecutivoList.stream().map(e->e.getPatio()).distinct().collect(Collectors.toList());
        List<Patio> finalPatioList=patioRepository.saveAll(patioList);
        ejecutivoList=ejecutivoList.stream().map(e->{
            e.setPatio(finalPatioList.stream().filter(p->p.getNombre().equals(e.getPatio().getNombre())).findFirst().get());
            return e;
        }).collect(Collectors.toList());
        return ejecutivoRepository.saveAll(ejecutivoList);
    }
}
