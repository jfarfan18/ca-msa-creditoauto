package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class RecordInUseException extends RuntimeException{

    public RecordInUseException(String message){
        super(message);
    }
}
