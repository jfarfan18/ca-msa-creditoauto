package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class SolicitudRegistrationFailException extends RuntimeException{

    public SolicitudRegistrationFailException(String message){
        super(message);
    }
}
