package com.bancopichincha.credito.automotriz;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.repository.*;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class CaMsaCreditoautoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaMsaCreditoautoApplication.class, args);
	}

}
