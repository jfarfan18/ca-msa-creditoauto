package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.service.VehiculoService;
import com.bancopichincha.credito.automotriz.service.dto.VehiculoDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehiculoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/vehiculo")
public class VehiculoController {

    @Autowired
    private VehiculoService vehiculoService;

    @Autowired
    private VehiculoMapper vehiculoMapper;

    @GetMapping("/{id}")
    public ResponseEntity<VehiculoDto> findById(@PathVariable Long id){
        Vehiculo vehiculo=vehiculoService.findById(id);
        return ResponseEntity.ok(vehiculoMapper.vehiculoToVehiculoDto(vehiculo));
    }

    @PostMapping
    public ResponseEntity<VehiculoDto> create(@Valid @RequestBody VehiculoDto vehiculoDto){
        Vehiculo vehiculoDb=vehiculoService.create(vehiculoMapper.VehiculoDtoToVehiculo(vehiculoDto));
        return ResponseEntity.created(URI.create("/vehiculo/".concat(vehiculoDb.getId().toString())))
                .body(vehiculoMapper.vehiculoToVehiculoDto(vehiculoDb));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VehiculoDto> update(@PathVariable Long id,@Valid @RequestBody VehiculoDto vehiculoDto){
        Vehiculo vehiculo = vehiculoService.update(id, vehiculoMapper.VehiculoDtoToVehiculo(vehiculoDto));
        return ResponseEntity.created(URI.create("/compania/".concat(vehiculoDto.getId().toString())))
                .body(vehiculoMapper.vehiculoToVehiculoDto(vehiculo));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        vehiculoService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<VehiculoDto>> findAll(){
        return ResponseEntity.ok(vehiculoMapper.vehiculoToVehiculoDtoAllDtos(vehiculoService.findAll()));
    }

    @GetMapping("/consultar")
    public ResponseEntity<List<VehiculoDto>> Consulta(@RequestParam(name="marca",required = false) Long idMarca,
                                                      @RequestParam(name="modelo",required = false) String modelo,
                                                      @RequestParam(name="anio",required = false) Integer anio){
        return ResponseEntity.ok(vehiculoMapper.vehiculoToVehiculoDtoAllDtos(vehiculoService.findByMarcaAndModeloAndAnio(idMarca,modelo,anio)));
    }

}
