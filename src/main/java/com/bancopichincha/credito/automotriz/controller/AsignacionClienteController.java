package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.service.AsignacionClienteService;
import com.bancopichincha.credito.automotriz.service.dto.AsignacionClienteDto;
import com.bancopichincha.credito.automotriz.service.mapper.AsignacionClienteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/asignacionCliente")
public class AsignacionClienteController {
    
    @Autowired
    private AsignacionClienteService asignacionClienteService;

    @Autowired
    private AsignacionClienteMapper asignacionClienteMapper;

    @GetMapping("/{id}")
    public ResponseEntity<AsignacionClienteDto> findById(@PathVariable Long id){
        AsignacionCliente asignacionCliente=asignacionClienteService.findById(id);
        return ResponseEntity.ok(asignacionClienteMapper.asignacionClienteToAsignacionClienteDto(asignacionCliente));
    }

    @PostMapping
    public ResponseEntity<AsignacionClienteDto> create(@Valid @RequestBody AsignacionClienteDto asignacionClienteDto){
        AsignacionCliente asignacionClienteDb=asignacionClienteService
                .create(asignacionClienteMapper.asignacionClienteDtoToAsignacionCliente(asignacionClienteDto));

        return ResponseEntity.created(URI.create("/asignacionCliente/".concat(asignacionClienteDb.getId().toString())))
                .body(asignacionClienteMapper.asignacionClienteToAsignacionClienteDto(asignacionClienteDb));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AsignacionClienteDto> update(@PathVariable Long id,@Valid @RequestBody AsignacionClienteDto asignacionClienteDto){
        AsignacionCliente asignacionCliente = asignacionClienteService.update(id, asignacionClienteMapper.asignacionClienteDtoToAsignacionCliente(asignacionClienteDto));
        return ResponseEntity.created(URI.create("/compania/".concat(asignacionClienteDto.getId().toString())))
                .body(asignacionClienteMapper.asignacionClienteToAsignacionClienteDto(asignacionCliente));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        asignacionClienteService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<AsignacionClienteDto>> findAll(){
        return ResponseEntity.ok(asignacionClienteMapper.asignacionClienteToAsignacionClienteDtoAllDtos(asignacionClienteService.findAll()));
    }
}
