package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/patio")
public class PatioController {
    
    @Autowired
    private PatioService patioService;

    @Autowired
    private PatioMapper patioMapper;

    @GetMapping("/{id}")
    public ResponseEntity<PatioDto> findById(@PathVariable Long id){
        Patio patio=patioService.findById(id);
        return ResponseEntity.ok(patioMapper.patioToPatioDto(patio));
    }

    @PostMapping
    public ResponseEntity<PatioDto> create(@Valid @RequestBody PatioDto patioDto){
        Patio patioDb=patioService.create(patioMapper.PatioDtoToPatio(patioDto));
        return ResponseEntity.created(URI.create("/patio/".concat(patioDb.getId().toString()))).body(patioMapper.patioToPatioDto(patioDb));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PatioDto> update(@PathVariable Long id,@Valid @RequestBody PatioDto patioDto){
        Patio patio = patioService.update(id, patioMapper.PatioDtoToPatio(patioDto));
        return ResponseEntity.created(URI.create("/compania/".concat(patio.getId().toString())))
                .body(patioMapper.patioToPatioDto(patio));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        patioService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<PatioDto>> findAll(){
        return ResponseEntity.ok(patioMapper.patioToPatioDtoAllDtos(patioService.findAll()));
    }
}
