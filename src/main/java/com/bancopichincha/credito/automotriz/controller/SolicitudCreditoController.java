package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.service.SolicitudCreditoService;
import com.bancopichincha.credito.automotriz.service.dto.SolicitudCreditoDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapper;
import com.bancopichincha.credito.automotriz.service.mapper.SolicitudCreditoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/solicitudCredito")
public class SolicitudCreditoController {
    
    @Autowired
    private SolicitudCreditoService solicitudCreditoService;

    @Autowired
    private SolicitudCreditoMapper solicitudCreditoMapper;

    @GetMapping("/{id}")
    public ResponseEntity<SolicitudCreditoDto> findById(@PathVariable Long id){
        SolicitudCredito solicitudCredito=solicitudCreditoService.findById(id);
        return ResponseEntity.ok(solicitudCreditoMapper.solicitudCreditoToSolicitudCreditoDto(solicitudCredito));
    }

    @PostMapping
    public ResponseEntity<SolicitudCreditoDto> create(@Valid @RequestBody SolicitudCreditoDto solicitudCreditoDto){
        SolicitudCredito solicitudCreditoDb = solicitudCreditoService.create(solicitudCreditoMapper.SolicitudCreditoDtoToSolicitudCredito(solicitudCreditoDto));
        return ResponseEntity.created(URI.create("/solicitudCredito/".concat(solicitudCreditoDb.getId().toString()))).body(solicitudCreditoMapper.solicitudCreditoToSolicitudCreditoDto(solicitudCreditoDb));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SolicitudCreditoDto> update(@PathVariable Long id,@Valid @RequestBody SolicitudCreditoDto solicitudCreditoDto){
        SolicitudCredito solicitudCredito = solicitudCreditoService.update(id, solicitudCreditoMapper.SolicitudCreditoDtoToSolicitudCredito(solicitudCreditoDto));
        return ResponseEntity.created(URI.create("/compania/".concat(solicitudCredito.getId().toString())))
                .body(solicitudCreditoMapper.solicitudCreditoToSolicitudCreditoDto(solicitudCredito));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        solicitudCreditoService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<SolicitudCreditoDto>> findAll(){
        return ResponseEntity.ok(solicitudCreditoMapper.solicitudCreditoToSolicitudCreditoDtoAllDtos(solicitudCreditoService.findAll()));
    }
}
