package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.Ejecutivo;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.repository.*;
import com.bancopichincha.credito.automotriz.service.VehiculoService;
import com.bancopichincha.credito.automotriz.service.mapper.VehiculoMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VehiculoController.class)
@ActiveProfiles("test")
class VehiculoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private VehiculoService vehiculoService;

    private ObjectMapper objectMapper;

    @Autowired
    private VehiculoMapper vehiculoMapper;

    @BeforeEach
    void setUp() {
        objectMapper=new ObjectMapper();
    }

    @Test
    void findById() throws Exception {
        when(vehiculoService.findById(anyLong())).thenReturn(Datos.VEHICULO_001);

        mvc.perform(get("/vehiculo/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(vehiculoService).findById(anyLong());
    }

    @Test
    void create() throws Exception {
        when(vehiculoService.create(any())).then(invocation->{
            Vehiculo v=invocation.getArgument(0);
            v.setId(1L);
            return v;
        });

        mvc.perform(post("/vehiculo").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(vehiculoMapper.vehiculoToVehiculoDto(Datos.VEHICULO_SIN_ID))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(vehiculoService).create(any(Vehiculo.class));
    }

    @Test
    void update() throws Exception {
        when(vehiculoService.update(anyLong(),any(Vehiculo.class))).thenReturn(Datos.VEHICULO_001);

        mvc.perform(put("/vehiculo/1").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(vehiculoMapper.vehiculoToVehiculoDto(Datos.VEHICULO_001))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(vehiculoService).update(anyLong(),any(Vehiculo.class));
    }

    @Test
    void deleteVehiculo() throws Exception {
        doNothing().when(vehiculoService).delete(anyLong());

        mvc.perform(delete("/vehiculo/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(vehiculoService).delete(anyLong());
    }

    @Test
    void findAll() throws Exception {

        when(vehiculoService.findAll()).thenReturn(Arrays.asList(Datos.VEHICULO_001));

        mvc.perform(get("/vehiculo").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(1)));

        verify(vehiculoService).findAll();
    }

    @Test
    void findByMarcaModeloAnio() throws Exception {

        when(vehiculoService.findByMarcaAndModeloAndAnio(anyLong(),anyString(),anyInt())).thenReturn(Arrays.asList(Datos.VEHICULO_001));

        mvc.perform(get("/vehiculo/consultar")
                        .param("marca","1")
                        .param("modelo","Vitara")
                        .param("anio","2020").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(1)));

        verify(vehiculoService).findByMarcaAndModeloAndAnio(anyLong(),anyString(),anyInt());
    }
}