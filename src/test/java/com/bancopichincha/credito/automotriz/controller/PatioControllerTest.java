package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PatioController.class)
@ActiveProfiles("test")
class PatioControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PatioService patioService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper=new ObjectMapper();
    }

    @Test
    void findById() throws Exception {
        when(patioService.findById(anyLong())).thenReturn(Datos.PATIO_001);

        mvc.perform(get("/patio/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(patioService).findById(anyLong());
    }

    @Test
    void create() throws Exception {
        when(patioService.create(any())).then(invocation->{
            Patio v=invocation.getArgument(0);
            v.setId(1L);
            return v;
        });

        mvc.perform(post("/patio").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Datos.PATIO_SIN_ID)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(patioService).create(any(Patio.class));
    }

    @Test
    void update() throws Exception {
        when(patioService.update(anyLong(),any(Patio.class))).thenReturn(Datos.PATIO_001);

        mvc.perform(put("/patio/1").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Datos.PATIO_001)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(patioService).update(anyLong(),any(Patio.class));
    }

    @Test
    void deletePatio() throws Exception {
        doNothing().when(patioService).delete(anyLong());

        mvc.perform(delete("/patio/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(patioService).delete(anyLong());
    }

    @Test
    void findAll() throws Exception {

        when(patioService.findAll()).thenReturn(Arrays.asList(Datos.PATIO_001));

        mvc.perform(get("/patio").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(1)));

        verify(patioService).findAll();
    }


}