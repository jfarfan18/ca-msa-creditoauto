package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.service.AsignacionClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AsignacionClienteController.class)
@ActiveProfiles("test")
class AsignacionClienteControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AsignacionClienteService asignacionClienteService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper=new ObjectMapper();
    }

    @Test
    void findById() throws Exception {
        when(asignacionClienteService.findById(anyLong())).thenReturn(Datos.ASIGNACION_CLIENTE_001);

        mvc.perform(get("/asignacionCliente/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(asignacionClienteService).findById(anyLong());
    }

    @Test
    void create() throws Exception {
        when(asignacionClienteService.create(any())).then(invocation->{
            AsignacionCliente v=invocation.getArgument(0);
            v.setId(1L);
            return v;
        });

        mvc.perform(post("/asignacionCliente").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Datos.ASIGNACION_CLIENTE_SIN_ID)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(asignacionClienteService).create(any(AsignacionCliente.class));
    }

    @Test
    void update() throws Exception {
        when(asignacionClienteService.update(anyLong(),any(AsignacionCliente.class))).thenReturn(Datos.ASIGNACION_CLIENTE_001);

        mvc.perform(put("/asignacionCliente/1").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Datos.ASIGNACION_CLIENTE_001)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));

        verify(asignacionClienteService).update(anyLong(),any(AsignacionCliente.class));
    }

    @Test
    void deleteAsignacionCliente() throws Exception {
        doNothing().when(asignacionClienteService).delete(anyLong());

        mvc.perform(delete("/asignacionCliente/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(asignacionClienteService).delete(anyLong());
    }

    @Test
    void findAll() throws Exception {

        when(asignacionClienteService.findAll()).thenReturn(Arrays.asList(Datos.ASIGNACION_CLIENTE_001));

        mvc.perform(get("/asignacionCliente").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(1)));

        verify(asignacionClienteService).findAll();
    }


}