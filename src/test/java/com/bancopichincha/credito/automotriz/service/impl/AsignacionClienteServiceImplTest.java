package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.AsignacionCliente;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.exception.RecordInUseException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.service.AsignacionClienteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class AsignacionClienteServiceImplTest {
    
    @MockBean
    AsignacionClienteRepository asignacionClienteRepository;
    
    @Autowired
    AsignacionClienteService asignacionClienteService;

    @Test
    void create() {
        when(asignacionClienteRepository.save(Datos.ASIGNACION_CLIENTE_SIN_ID)).then(invocationOnMock ->{
            AsignacionCliente asignacionCliente=invocationOnMock.getArgument(0);
            asignacionCliente.setId(100L);
            return asignacionCliente;
        });

        AsignacionCliente asignacionCliente=asignacionClienteService.create(Datos.ASIGNACION_CLIENTE_SIN_ID);

        assertEquals(100L,asignacionCliente.getId());
        verify(asignacionClienteRepository).save(any());
    }


    @Test
    void update() {
        when(asignacionClienteRepository.save(Datos.ASIGNACION_CLIENTE_001)).thenReturn(Datos.ASIGNACION_CLIENTE_001);
        when(asignacionClienteRepository.findById(1L)).thenReturn(Optional.of(Datos.ASIGNACION_CLIENTE_001));

        AsignacionCliente asignacionCliente=asignacionClienteService.update(1L,Datos.ASIGNACION_CLIENTE_001);

        assertEquals(1L,asignacionCliente.getId());
        verify(asignacionClienteRepository).findById(any());
        verify(asignacionClienteRepository).save(any());
    }

    @Test
    void updateThrowsNotFoundException() {
        when(asignacionClienteRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, ()->{asignacionClienteService.update(2L,Datos.ASIGNACION_CLIENTE_001);});
    }

    @Test
    void delete() {
        when(asignacionClienteRepository.findById(1L)).thenReturn(Optional.of(Datos.ASIGNACION_CLIENTE_001));
        doNothing().when(asignacionClienteRepository).delete(any());

        asignacionClienteService.delete(1L);

        verify(asignacionClienteRepository).findById(any());
        verify(asignacionClienteRepository).delete(any());

    }


    @Test
    void findById() {
        when(asignacionClienteRepository.findById(1L)).thenReturn(Optional.of(Datos.ASIGNACION_CLIENTE_001));

        AsignacionCliente asignacionCliente=asignacionClienteService.findById(1L);

        assertEquals(1L,asignacionCliente.getId());
        verify(asignacionClienteRepository).findById(1L);
    }

    @Test
    void findAll() {
        when(asignacionClienteRepository.findAll()).thenReturn(Arrays.asList(Datos.ASIGNACION_CLIENTE_001));

        List<AsignacionCliente> asignacionClienteList=asignacionClienteService.findAll();

        assertFalse(asignacionClienteList.isEmpty());
        assertEquals(1,asignacionClienteList.size());
        verify(asignacionClienteRepository).findAll();
    }
}