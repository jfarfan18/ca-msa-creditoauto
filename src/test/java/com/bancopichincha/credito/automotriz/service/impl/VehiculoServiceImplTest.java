package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.exception.RecordInUseException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.exception.UniqueViolationException;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.repository.VehiculoRepository;
import com.bancopichincha.credito.automotriz.service.VehiculoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class VehiculoServiceImplTest {

    @MockBean
    VehiculoRepository vehiculoRepository;

    @MockBean
    SolicitudCreditoRepository solicitudCreditoRepository;

    @Autowired
    VehiculoService vehiculoService;

    @Test
    void create() {
        when(vehiculoRepository.save(Datos.VEHICULO_SIN_ID)).then(invocationOnMock ->{
            Vehiculo vehiculo=invocationOnMock.getArgument(0);
            vehiculo.setId(100L);
            return vehiculo;
        });

        Vehiculo vehiculo=vehiculoService.create(Datos.VEHICULO_SIN_ID);

        assertEquals(100L,vehiculo.getId());
        verify(vehiculoRepository).save(any());
    }

    @Test
    void createThrowsUniqueViolationException() {
        when(vehiculoRepository.findByPlaca(any())).thenReturn(Optional.of(Datos.VEHICULO_001));

        assertThrows(UniqueViolationException.class,()->{vehiculoService.create(Datos.VEHICULO_SIN_ID);});
    }

    @Test
    void update() {
        when(vehiculoRepository.save(Datos.VEHICULO_001)).thenReturn(Datos.VEHICULO_001);
        when(vehiculoRepository.findById(1L)).thenReturn(Optional.of(Datos.VEHICULO_001));

        Vehiculo vehiculo=vehiculoService.update(1L,Datos.VEHICULO_001);

        assertEquals(1L,vehiculo.getId());
        verify(vehiculoRepository).findById(any());
        verify(vehiculoRepository).save(any());
    }

    @Test
    void updateThrowsNotFoundException() {
        when(vehiculoRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, ()->{vehiculoService.update(2L,Datos.VEHICULO_001);});
    }

    @Test
    void findByMarcaAndModeloAndAnio() {
        when(vehiculoRepository.consultaMarcaModeloAnio(any(),any(),any())).thenReturn(Arrays.asList(Datos.VEHICULO_001));

        List<Vehiculo> vehiculos = vehiculoService.findByMarcaAndModeloAndAnio(1L, "Vitara", 2020);

        assertEquals(1,vehiculos.size());
        verify(vehiculoRepository).consultaMarcaModeloAnio(any(),any(),any());
    }

    @Test
    void delete() {
        when(vehiculoRepository.findById(1L)).thenReturn(Optional.of(Datos.VEHICULO_001));
        when(solicitudCreditoRepository.findByVehiculo(any(Vehiculo.class))).thenReturn(new ArrayList<>());
        doNothing().when(vehiculoRepository).delete(any());

        vehiculoService.delete(1L);

        verify(vehiculoRepository).findById(any());
        verify(solicitudCreditoRepository).findByVehiculo(any());
        verify(vehiculoRepository).delete(any());

    }

    @Test
    void deleteThrowsrecordInUseException() {
        when(vehiculoRepository.findById(1L)).thenReturn(Optional.of(Datos.VEHICULO_001));
        when(solicitudCreditoRepository.findByVehiculo(any(Vehiculo.class))).thenReturn(Arrays.asList(Datos.SOLICITUD_CREDITO_001));

        assertThrows(RecordInUseException.class,()->{vehiculoService.delete(1L);});


    }

    @Test
    void findById() {
        when(vehiculoRepository.findById(1L)).thenReturn(Optional.of(Datos.VEHICULO_001));

        Vehiculo vehiculo=vehiculoService.findById(1L);

        assertEquals(1L,vehiculo.getId());
        verify(vehiculoRepository).findById(1L);
    }

    @Test
    void findAll() {
        when(vehiculoRepository.findAll()).thenReturn(Arrays.asList(Datos.VEHICULO_001));

        List<Vehiculo> vehiculoList=vehiculoService.findAll();

        assertFalse(vehiculoList.isEmpty());
        assertEquals(1,vehiculoList.size());
        verify(vehiculoRepository).findAll();
    }
}