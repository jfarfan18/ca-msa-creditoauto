package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.SolicitudCredito;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.service.SolicitudCreditoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
class SolicitudCreditoServiceImplTest {

    @MockBean
    SolicitudCreditoRepository solicitudCreditoRepository;

    @Autowired
    SolicitudCreditoService solicitudCreditoService;

    @Test
    void create() {
        when(solicitudCreditoRepository.save(Datos.SOLICITUD_CREDITO_SIN_ID)).then(invocationOnMock ->{
            SolicitudCredito solicitudCredito=invocationOnMock.getArgument(0);
            solicitudCredito.setId(100L);
            return solicitudCredito;
        });

        SolicitudCredito solicitudCredito=solicitudCreditoService.create(Datos.SOLICITUD_CREDITO_SIN_ID);

        assertEquals(100L,solicitudCredito.getId());
        verify(solicitudCreditoRepository).save(any());
    }


    @Test
    void update() {
        when(solicitudCreditoRepository.save(Datos.SOLICITUD_CREDITO_001)).thenReturn(Datos.SOLICITUD_CREDITO_001);
        when(solicitudCreditoRepository.findById(1L)).thenReturn(Optional.of(Datos.SOLICITUD_CREDITO_001));

        SolicitudCredito solicitudCredito=solicitudCreditoService.update(1L,Datos.SOLICITUD_CREDITO_001);

        assertEquals(1L,solicitudCredito.getId());
        verify(solicitudCreditoRepository).findById(any());
        verify(solicitudCreditoRepository).save(any());
    }

    @Test
    void updateThrowsNotFoundException() {
        when(solicitudCreditoRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, ()->{solicitudCreditoService.update(2L,Datos.SOLICITUD_CREDITO_001);});
    }

    @Test
    void delete() {
        when(solicitudCreditoRepository.findById(1L)).thenReturn(Optional.of(Datos.SOLICITUD_CREDITO_001));
        doNothing().when(solicitudCreditoRepository).delete(any());

        solicitudCreditoService.delete(1L);

        verify(solicitudCreditoRepository).findById(any());
        verify(solicitudCreditoRepository).delete(any());

    }


    @Test
    void findById() {
        when(solicitudCreditoRepository.findById(1L)).thenReturn(Optional.of(Datos.SOLICITUD_CREDITO_001));

        SolicitudCredito solicitudCredito=solicitudCreditoService.findById(1L);

        assertEquals(1L,solicitudCredito.getId());
        verify(solicitudCreditoRepository).findById(1L);
    }

    @Test
    void findAll() {
        when(solicitudCreditoRepository.findAll()).thenReturn(Arrays.asList(Datos.SOLICITUD_CREDITO_001));

        List<SolicitudCredito> solicitudCreditoList=solicitudCreditoService.findAll();

        assertFalse(solicitudCreditoList.isEmpty());
        assertEquals(1,solicitudCreditoList.size());
        verify(solicitudCreditoRepository).findAll();
    }

}