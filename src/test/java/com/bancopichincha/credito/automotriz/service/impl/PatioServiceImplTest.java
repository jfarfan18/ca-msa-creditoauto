package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.Datos;
import com.bancopichincha.credito.automotriz.domain.Patio;
import com.bancopichincha.credito.automotriz.domain.Vehiculo;
import com.bancopichincha.credito.automotriz.exception.RecordInUseException;
import com.bancopichincha.credito.automotriz.exception.RecordNotFoundException;
import com.bancopichincha.credito.automotriz.exception.UniqueViolationException;
import com.bancopichincha.credito.automotriz.repository.AsignacionClienteRepository;
import com.bancopichincha.credito.automotriz.repository.EjecutivoRepository;
import com.bancopichincha.credito.automotriz.repository.PatioRepository;
import com.bancopichincha.credito.automotriz.repository.SolicitudCreditoRepository;
import com.bancopichincha.credito.automotriz.service.PatioService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class PatioServiceImplTest {

    @MockBean
    PatioRepository patioRepository;

    @MockBean
    SolicitudCreditoRepository solicitudCreditoRepository;

    @MockBean
    EjecutivoRepository ejecutivoRepository;

    @MockBean
    AsignacionClienteRepository asignacionClienteRepository;

    @Autowired
    PatioService patioService;

    @Test
    void create() {
        when(patioRepository.save(Datos.PATIO_SIN_ID)).then(invocationOnMock ->{
            Patio patio=invocationOnMock.getArgument(0);
            patio.setId(100L);
            return patio;
        });

        Patio patio=patioService.create(Datos.PATIO_SIN_ID);

        assertEquals(100L,patio.getId());
        verify(patioRepository).save(any());
    }


    @Test
    void update() {
        when(patioRepository.save(Datos.PATIO_001)).thenReturn(Datos.PATIO_001);
        when(patioRepository.findById(1L)).thenReturn(Optional.of(Datos.PATIO_001));

        Patio patio=patioService.update(1L,Datos.PATIO_001);

        assertEquals(1L,patio.getId());
        verify(patioRepository).findById(any());
        verify(patioRepository).save(any());
    }

    @Test
    void updateThrowsNotFoundException() {
        when(patioRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, ()->{patioService.update(2L,Datos.PATIO_001);});
    }

    @Test
    void delete() {
        when(patioRepository.findById(1L)).thenReturn(Optional.of(Datos.PATIO_001));
        when(solicitudCreditoRepository.findByPatio(any(Patio.class))).thenReturn(new ArrayList<>());
        when(ejecutivoRepository.findByPatio(any(Patio.class))).thenReturn(new ArrayList<>());
        when(asignacionClienteRepository.findByPatio(any(Patio.class))).thenReturn(new ArrayList<>());
        doNothing().when(patioRepository).delete(any());

        patioService.delete(1L);

        verify(patioRepository).findById(any());
        verify(solicitudCreditoRepository).findByPatio(any());
        verify(ejecutivoRepository).findByPatio(any());
        verify(asignacionClienteRepository).findByPatio(any());
        verify(patioRepository).delete(any());

    }

    @Test
    void deleteThrowsrecordInUseException() {
        when(patioRepository.findById(1L)).thenReturn(Optional.of(Datos.PATIO_001));
        when(solicitudCreditoRepository.findByPatio(any(Patio.class))).thenReturn(Arrays.asList(Datos.SOLICITUD_CREDITO_001));

        assertThrows(RecordInUseException.class,()->{patioService.delete(1L);});


    }

    @Test
    void findById() {
        when(patioRepository.findById(1L)).thenReturn(Optional.of(Datos.PATIO_001));

        Patio patio=patioService.findById(1L);

        assertEquals(1L,patio.getId());
        verify(patioRepository).findById(1L);
    }

    @Test
    void findAll() {
        when(patioRepository.findAll()).thenReturn(Arrays.asList(Datos.PATIO_001));

        List<Patio> patioList=patioService.findAll();

        assertFalse(patioList.isEmpty());
        assertEquals(1,patioList.size());
        verify(patioRepository).findAll();
    }
}