package com.bancopichincha.credito.automotriz;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.domain.enums.EstadoSolicitudCredito;

import java.math.BigDecimal;
import java.util.Date;

public class Datos {

    public static final Marca MARCA_SIN_ID= new Marca(null,"CHEVROLET");

    public static final Marca MARCA_001= new Marca(1L,"TOYOTA");

    public static final Vehiculo VEHICULO_SIN_ID= new Vehiculo(null, "ABC0001","Gran Vitara", "0001",2020,null, 2000,new BigDecimal("1500"),MARCA_001);

    public static final Vehiculo VEHICULO_001= new Vehiculo(1L, "ABC0002","Yaris", "0002",2020,null, 2000,new BigDecimal("1500"),MARCA_001);

    public static final Cliente CLIENTE_SIN_ID=new Cliente(null,true,new Persona());

    public static final Cliente CLIENTE_001=new Cliente(1L,true,new Persona());

    public static final Patio PATIO_SIN_ID=new Patio(null,"Patio 000","Calle Lamar 3-56 y Borrero", "022658565","000");

    public static final Patio PATIO_001=new Patio(1L,"Patio 001","Juan Diaz 4-65 y Calle Vieja", "022326514","001");

    public static final Ejecutivo EJECUTIVO_SIN_ID=new Ejecutivo(null,PATIO_001,new Persona());

    public static final Ejecutivo EJECUTIVO_001=new Ejecutivo(1L,PATIO_001,new Persona());

    public static final SolicitudCredito SOLICITUD_CREDITO_SIN_ID= new SolicitudCredito(null,new Date(),12,new BigDecimal(1000),new BigDecimal(1000),CLIENTE_001,PATIO_001,VEHICULO_001,EJECUTIVO_001, EstadoSolicitudCredito.REGISTRADA,"");

    public static final SolicitudCredito SOLICITUD_CREDITO_001= new SolicitudCredito(1L,new Date(),12,new BigDecimal(1000),new BigDecimal(1000),CLIENTE_001,PATIO_001,VEHICULO_001,EJECUTIVO_001, EstadoSolicitudCredito.REGISTRADA,"");

    public static final AsignacionCliente ASIGNACION_CLIENTE_SIN_ID=new AsignacionCliente(null,CLIENTE_001,PATIO_001,new Date());

    public static final AsignacionCliente ASIGNACION_CLIENTE_001=new AsignacionCliente(1L,CLIENTE_001,PATIO_001,new Date());

}
